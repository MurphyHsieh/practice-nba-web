import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyC8xNr2WXUwW4DwJKQoVyGlxnieNfM3TMM",
    authDomain: "nba-full-d98f4.firebaseapp.com",
    databaseURL: "https://nba-full-d98f4.firebaseio.com",
    projectId: "nba-full-d98f4",
    storageBucket: "nba-full-d98f4.appspot.com",
    messagingSenderId: "467184340057"
};
firebase.initializeApp(config);

// 建立簡寫
const firebaseDB = firebase.database();
const firebaseArticles = firebaseDB.ref('articles');
const firebaseTeams = firebaseDB.ref('teams');
const firebaseVideos = firebaseDB.ref('videos');

const firebaseLooper = (snapshot) => { // 把每次都要轉成陣列的方法儲存成 reusable function
    const data = [];
    snapshot.forEach(childSnapshot => {
        data.push({
            ...childSnapshot.val(),
            id: childSnapshot.key // 這個 id 會儲存文章在 firebase獨特的 id
        })
    })
    return data;
}

export {
    firebase,
    firebaseDB,
    firebaseArticles,
    firebaseTeams,
    firebaseVideos,
    firebaseLooper,
}