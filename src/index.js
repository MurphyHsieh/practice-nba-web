import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import { firebase } from './firebase';

import Routes from './routes';

const App = (props) => { // 得知是否有登入，重要的一環
    return (
        <BrowserRouter>
            <Routes {...props} />
        </BrowserRouter>
    )
}
firebase.auth().onAuthStateChanged(user => { // 得知是否有登入，重要的一環
    ReactDOM.render(<App user={user} />, document.getElementById('root'));
})


