import React, { Component } from 'react';
import './layout.css';
import Header from '../../component/Header/header';
import Footer from '../../component/Footer/footer';

class Layout extends Component {

    state = {
        showNav: false
    }

    toggleSideNav = (action) => {
        this.setState({
            showNav: action
        })
    }

    render() {
        return (
            <div>
                <Header
                    user={this.props.user}
                    showNav={this.state.showNav}
                    onHideNav={() => this.toggleSideNav(false)} // 傳參數 false，關閉 sidenav
                    onOpenNav={() => this.toggleSideNav(true)} // 傳參數 true，打開 sidenav
                />
                {this.props.children}
                <Footer />
            </div>
        )
    }
}

export default Layout;