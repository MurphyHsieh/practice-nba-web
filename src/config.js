const CURRENT_YEAR = (new Date()).getFullYear();
const URL = 'http://localhost:3004';

export {
    CURRENT_YEAR, // 等同於 CURRENT_YEAR: CURRENT_YEAR
    URL
}