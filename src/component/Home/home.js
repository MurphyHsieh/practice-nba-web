import React from 'react';
import NewsSlider from '../widgets/NewsSlider/slider';
import NewsList from '../widgets/NewsList/newsList';
import VideosList from '../widgets/VideosList/videosList';

const Home = () => {
    return (
        <div>
            <NewsSlider
                type="featured"
                start={0} // 從第幾張開始播
                amount={5} // 播到第幾張
                setting={{    // 第二個大括弧告訴他這是個物件！
                    dots: false
                }}
            />
            <NewsList
                type="card"
                loadmore={true}
                start={0}
                amount={3}
            />
            <VideosList
                type="card"
                title={true}
                loadmore={true} // false 會秀出 More Video button
                start={0}
                amount={3}
            />
        </div>
    )
};

export default Home;
