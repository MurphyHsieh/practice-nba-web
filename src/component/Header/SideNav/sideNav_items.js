import React from 'react';
import style from './sideNav.css';
import { firebase } from '../../../firebase';
import FontAwesome from 'react-fontawesome';

import { Link, withRouter } from 'react-router-dom';
// 不是真正的 router 沒有 props.history
// 可以用 { withRouter } 使得擁有 router 功能
// inject the routes information inside this component 

const SideNavItems = (props) => {
    console.log(props)
    const items = [
        {
            type: style.option, // main purpose: 未來想要多變的 icon
            icon: 'home',
            text: 'Home',
            link: '/',
            login: ''
        },
        {
            type: style.option,
            icon: 'file-text-o',
            text: 'News',
            link: '/news',
            login: ''
        },
        {
            type: style.option,
            icon: 'play',
            text: 'Videos',
            link: '/videos',
            login: ''
        },
        {
            type: style.option,
            icon: 'sign-in',
            text: 'Dashboard',
            link: '/dashboard',
            login: false
        },
        {
            type: style.option,
            icon: 'sign-in',
            text: 'Sign in',
            link: '/sign-in',
            login: true
        },
        {
            type: style.option,
            icon: 'sign-out',
            text: 'Sign out',
            link: '/sign-out',
            login: false
        }
    ]


    const element = (item, i) => (
        <div key={i} className={item.type}>
            <Link to={item.link}>
                <FontAwesome name={item.icon} />
                {item.text}
            </Link>
        </div>
    )

    const restricted = (item, i) => {
        let template = null;

        if (props.user === null && item.login) {
            template = element(item, i)
        }

        if (props.user !== null && !item.login) {
            if (item.link === '/sign-out') {
                template = (
                    <div
                        key={i}
                        className={item.type}
                        onClick={() => {
                            firebase.auth().signOut()
                                .then(() => {
                                    props.history.push("/")
                                })
                        }}
                    >
                        <FontAwesome name={item.icon} />
                        {item.text}
                    </div>
                )
            } else {
                template = element(item, i)
            }
        }

        return template
    }


    const showItems = () => {
        return items.map((item, i) => {
            return item.login !== '' ?
                restricted(item, i)
                :
                element(item, i)
        })
    }

    return (
        <div>
            {showItems()}
        </div>
    )
};

export default withRouter(SideNavItems);