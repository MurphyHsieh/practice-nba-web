import React from 'react';
import style from './header.css';

import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome'; // 不用大括弧包住 FontAwesome
import SideNav from './SideNav/sideNav';

const Header = (props) => {

    const navBars = () => (
        <div>
            <FontAwesome name="bars"
                onClick={props.onOpenNav} // sideNav 預設是關閉的，點擊這個 icon會打開
                style={{
                    color: "#dfdfdf",
                    padding: '10px',
                    cursor: 'pointer' // 類似hover效果，pointer 變成一根手指頭
                }}
            />
        </div>
    )

    const logo = () => (
        <Link to="/" className={style.logo}>
            <img alt="nbalogo" src="/images/nba_logo.png" />
        </Link>
    )

    return (
        <header className={style.header}>
            <SideNav {...props} />
            <div className={style.headerOpt}>
                {navBars()}
                {logo()}
            </div>
        </header>
    )

}

export default Header;

