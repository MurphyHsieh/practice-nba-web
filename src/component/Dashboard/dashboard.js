import React, { Component } from 'react';
import styles from './dashboard.css';

import FormField from '../widgets/FormsFields/formFields';
import { firebaseTeams, firebaseArticles, firebase } from '../../firebase';

import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import { stateToHTML } from 'draft-js-export-html';
import Uploader from '../widgets/FileUploader/fileUploader';

class Dashboard extends Component {

    state = {
        // use editor state，Editor 開始的時候，會開始尋找這個 editorState
        editorState: EditorState.createEmpty(),

        // down below basically, just like sign in 
        postError: '',
        loading: false,
        formdata: {
            author: {
                element: 'input',
                value: '',
                config: {
                    name: 'author_input',
                    type: 'text',
                    placeholder: 'Enter your name'
                },
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                validationMessage: ''
            },
            title: {
                element: 'input',
                value: '',
                config: {
                    name: 'title_input',
                    type: 'text',
                    placeholder: 'Enter the title'
                },
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                validationMessage: ''
            },
            body: {
                element: 'texteditor',
                valie: '',
                valid: true
            },
            image: {
                element: 'image',
                valie: '',
                valid: true
            },
            team: {
                element: 'select',
                value: '',
                config: {
                    name: 'teams_input',
                    options: []
                },
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                validationMessage: ''
            }
        }
    }

    componentDidMount() {
        this.loadTeams()
    }

    loadTeams = () => {
        firebaseTeams.once('value')
            .then((snapshot) => {
                let team = []

                snapshot.forEach((childSnaphot) => {
                    team.push({
                        id: childSnaphot.val().teamId,
                        city: childSnaphot.val().city
                    })
                })


                const newFormdata = { ...this.state.formdata };
                const newElement = { ...newFormdata['team'] };

                newElement.config.options = team;
                newFormdata['team'] = newElement;

                this.setState({ formdata: newFormdata })

            })
    }

    updateForm = (element, content = '') => { // ES6 函式參數宣告初始值後，可以不必硬傳東西，他會用初始值！

        const newFormdata = {
            ...this.state.formdata
        }
        const newElement = {
            ...newFormdata[element.id]
        }

        // 做個判斷，若 content為 ''(default) 代表他是從 FormField 來
        if (content === '') {
            newElement.value = element.event.target.value;
        } else { // content不為 '' 則代表它是由 Editor 來
            newElement.value = content;
        }

        if (element.blur) {
            let validData = this.validate(newElement);
            newElement.valid = validData[0];
            newElement.validationMessage = validData[1];
        }
        newElement.touched = element.blur;
        newFormdata[element.id] = newElement;
        this.setState({ formdata: newFormdata })
    }

    validate = (element) => {  // validate 很重要，但裡面的「運算子使用」簡化程式碼更重要
        let error = [true, ''];

        if (element.validation.required) {
            const valid = element.value.trim() !== '';
            const message = `${!valid ? 'These field is required' : ''} `;
            error = !valid ? [valid, message] : error;
        }
        return error;
    }


    submitForm = (event) => {
        // event for preventDefault(), type for check if this log in or register
        event.preventDefault();

        let dataToSubmit = {};
        let formIsValid = true;

        for (let key in this.state.formdata) {
            dataToSubmit[key] = this.state.formdata[key].value;
        }
        for (let key in this.state.formdata) {
            formIsValid = this.state.formdata[key].valid && formIsValid;
        }

        console.log(dataToSubmit)

        if (formIsValid) {
            // firebase 資料上傳後，id 不會自己 increment by 1（痛點）
            // 解決方法，把資料抓下來手動加一
            this.setState({
                loading: true,
                postError: ''
            })

            firebaseArticles.orderByChild('id')
                .limitToLast(1).once('value')
                .then(snapshot => {
                    let articleId = null;
                    snapshot.forEach(childSnaphot => {
                        articleId = childSnaphot.val().id;
                    });

                    // 設定上傳的時間（服務器當下的時間 server time ）
                    // create a new key in dataToSubmit 
                    dataToSubmit['date'] = firebase.database.ServerValue.TIMESTAMP
                    dataToSubmit['id'] = articleId + 1;
                    dataToSubmit['team'] = parseInt(dataToSubmit['team'], 10);


                    // 一切都設定完成後，就可以 push 資料到 firebase上面了
                    firebaseArticles.push(dataToSubmit)
                        .then(article => { // 成功上傳後，要重新導向使用者到那個 article
                            this.props.history.push(`/articles/${article.key}`)
                        }).catch(e => {
                            this.setState({
                                postError: e.message
                            })
                        })



                })







        } else {
            this.setState({
                loading: false,
                postError: 'Something went wrong'
            })
        }
    }

    submitButton = () => (
        this.state.loading ?
            'loading...'
            :
            <div>
                <button type="submit"> Add Post</button>
            </div>
    )

    showError = () => (
        this.state.postError !== '' ?
            <div className={styles.error}>{this.state.postError}</div>
            : ''
    )

    onEditorStateChange = (editorState) => {
        // <Editor> 的 onEditorStateChange 有this.state.editorStatet所需要的東西！

        //
        // 轉換輸入的東西變成可以秀到網頁上的文字
        // 第一種：
        // convert to html 儲存到 firebase
        // 好處：可以在 firebase 看到儲存的資料
        let contentState = editorState.getCurrentContent();
        let html = stateToHTML(contentState)

        // 第二種：
        // convert to raw (json file)儲存起來，要用的時候再抓下來 cenvert to html 
        // 老師本身會使用第二種方法！
        // let rawState = convertToRaw(contentState);


        // 讓輸入的東西 觸發 updateForm 更新 formdata
        // 首先必須要到 state 的 formdata 新增body
        this.updateForm({ id: 'body' }, html)
        this.setState({
            editorState
        })
    }

    storeFilename = (filename) => { // 當作 props 傳去給 fileUploader 並接受 filename的回傳
        this.updateForm({ id: 'image' }, filename)
    }

    render() {
        return (
            <div className={styles.postContainer}>
                <form onSubmit={this.submitForm}>

                    <h2>Add Post</h2>

                    <Uploader
                        filename={(filename) => this.storeFilename(filename)}
                    />

                    <FormField
                        id={'author'}
                        formdata={this.state.formdata.author}
                        change={(element) => this.updateForm(element)}
                    />

                    <FormField
                        id={'title'}
                        formdata={this.state.formdata.title} // for config and input value
                        change={(element) => this.updateForm(element)}
                    />

                    <Editor
                        editorState={this.state.editorState}
                        wrapperClassName="myEditor-wrapper"
                        editorClassName="myEditor-editor"
                        onEditorStateChange={this.onEditorStateChange}
                    />

                    <FormField
                        id={'team'}
                        formdata={this.state.formdata.team}
                        change={(element) => this.updateForm(element)}
                    />

                    {this.submitButton()}
                    {this.showError()}
                </form>
            </div>
        )
    }
};

export default Dashboard;
