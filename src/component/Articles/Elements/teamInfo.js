import React from 'react';
import styles from '../articles.css';

const TeamInfo = (props) => { // 兩個主要部分。

    return (
        <div className={styles.articleTeamHeader} >
            <div className={styles.left}
                style={{
                    background: `url('/images/teams/${props.team.logo}')`
                }}
            >
                <div></div>
            </div>
            <div className={styles.right}>
                <div>
                    <span>{props.team.city}{props.team.name}</span>
                </div>
                <div>
                    <strong>
                        W{props.team.stats[0].wins}-w
                        L{props.team.stats[0].defeats}-l
                    </strong>
                </div>
            </div>
        </div>
    )
}

export default TeamInfo;
