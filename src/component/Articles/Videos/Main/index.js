import React from 'react';
import VideosList from '../../../widgets/VideosList/videosList';
import styles from '../../articles.css'

const VideoMain = () => {
    return (
        <div className={styles.videoMain_wrapper}>
            <VideosList
                type="card"
                title={false}
                loadmore={true} // false 會秀出 More Video button
                start={0}
                amount={10}
            />
        </div>
    )
};

export default VideoMain;