import React, { Component } from 'react';
import styles from '../../articles.css';
import Header from './header';
import VideoRelated from '../../../widgets/VideosList/VideosRelated/videosRelated';
import { firebaseDB, firebaseVideos, firebaseTeams, firebaseLooper } from '../../../../firebase';



class VideoArticle extends Component {

    state = {
        article: [],
        team: [],
        teams: [],
        related: []
    }

    componentWillMount() {
        firebaseDB.ref(`videos/${this.props.match.params.id}`).once('value')
            .then(snapshot => {
                let article = snapshot.val()
                firebaseTeams.orderByChild('id').equalTo(article.team).once('value')
                    .then(snapshot => {
                        let team = firebaseLooper(snapshot)
                        this.setState({
                            article,
                            team
                        })
                        this.getRelated();
                    })
            })


        /* ========== without firebase ========== */

        // axios.get(`${URL}/videos?id=${this.props.match.params.id}`)
        //     .then(response => {
        //         let article = response.data[0];

        //         axios.get(`${URL}/teams?id=${article.team}`)
        //             .then(response => {
        //                 this.setState({
        //                     article,
        //                     team: response.data
        //                 });
        //                 this.getRelated(); // 得到 related video
        //             })

        //     })
    }

    getRelated = () => {

        // firebase 本身是沒有支援 ?q= 這個的，但還是有其他方法可以達成
        firebaseTeams.once('value')
            .then(snapshot => {
                let teams = firebaseLooper(snapshot);
                firebaseVideos.orderByChild('team').equalTo(this.state.article.team).limitToFirst(3).once('value')
                    .then(snapshot => {
                        let related = firebaseLooper(snapshot)
                        this.setState({
                            related,
                            teams
                        })
                    })
            })



        /* ========== without firebase ========== */

        // 得到 related video
        // 透過網址後面加 ?q=城市的名字 

        // axios.get(`${URL}/teams`)
        //     .then(response => {
        //         let teams = response.data

        //         axios.get(`${URL}/videos?q=${this.state.team[0].city}&_limit=3`)
        //             .then(response => {
        //                 this.setState({
        //                     teams,
        //                     related: response.data
        //                 })
        //             })
        //     })
    }

    render() {
        const article = this.state.article;
        const team = this.state.team
        return (
            <div>
                <Header teamData={team[0]} />
                <div className={styles.videoWrapper}>
                    <h1>{article.title}</h1>
                    <iframe
                        // 之後實際操作就要把 <iframe> 宣告成 component，因為之後會有其他功能
                        title="videoplayer"
                        width="100%"
                        height="300px"
                        src={`http://www.youtube.com/embed/${article.url}`}
                    >
                    </iframe>
                </div>
                <VideoRelated
                    data={this.state.related}
                    teams={this.state.teams}
                />
            </div>
        )
    }
};


export default VideoArticle;
