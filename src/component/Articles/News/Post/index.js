import React, { Component } from 'react';
import styles from '../../articles.css';
import Header from './header';

import { firebase, firebaseDB, firebaseLooper, firebaseTeams } from '../../../../firebase';

class NewsArticles extends Component {

    state = {
        article: [],
        team: [],
        imageURL: ''
    }


    componentWillMount() {
        firebaseDB.ref(`articles/${this.props.match.params.id}`).once('value')
            .then(snapshot => {
                let article = snapshot.val();
                firebaseTeams.orderByChild('id').equalTo(article.team).once('value')
                    .then(snapshot => {
                        const team = firebaseLooper(snapshot)
                        this.setState({
                            article,
                            team
                        })
                        // 在這之後去進行 getImageURL，傳入儲存在 article裡的圖片名字
                        this.getImageURL(article.image)
                    })
            })
    }


    getImageURL = (filename) => { // 這個會導致畫面 render 兩次，可以試試看其他方法！
        firebase.storage().ref('images')
            .child(filename).getDownloadURL()
            .then(url => {
                this.setState({
                    imageURL: url
                })
            })
    }



    /* ========== without firebase ========== */

    // componentWillMount() { // 網址後面加參數記得寫 ？參數=
    //     axios.get(`${URL}/articles?id=${this.props.match.params.id}`)
    //         .then(response => {
    //             let article = response.data[0];
    //             axios.get(`${URL}/teams?id=${article.team}`)
    //                 .then(response => {
    //                     this.setState({
    //                         article,
    //                         team: response.data
    //                     })
    //                 })
    //         })
    // }

    /* ========== without firebase use async-await ========== */

    // async getArticle() {
    //     let res = await fetch(`${URL}/articles?id=${this.props.match.params.id}`)
    //     let data = await res.json();
    //     return await data;

    // }
    // async getTeam(article) {
    //     let res = await fetch(`${URL}/teams?id=${article[0].team}`)
    //     let data = await res.json();
    //     return await data
    // }
    // async componentDidMount() { // 網址後面加參數記得寫 ？參數=
    //     let data = await this.getArticle()
    //     this.setState({
    //         article: data[0]
    //     })
    //     let team = await this.getTeam(data)
    //     this.setState({
    //         team: team
    //     })
    // }



    render() {
        const article = this.state.article; // 傳 props 的時候可以寫比較少字
        const team = this.state.team;
        return (
            <div className={styles.article_wrapper}>
                <Header
                    teamData={team[0]}
                    date={article.date}
                    author={article.author}
                />
                {/* article body */}
                <div className={styles.articleBody}>
                    <h1>{article.title}</h1>
                    <div className={styles.articleImage}
                        style={{
                            background: `url('${this.state.imageURL}')`
                        }}
                    >
                        <div></div>
                    </div>
                    <div className={styles.articleText}
                        dangerouslySetInnerHTML={{
                            __html: article.body // 可以讓變數的內容當 html 畫出來！
                        }}
                    >
                        {/* article.body */}
                    </div>
                </div>
            </div>
        )
    }
};

export default NewsArticles;
