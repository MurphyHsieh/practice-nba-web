import React from 'react';
import TeamInfo from '../../Elements/teamInfo';
import PostData from '../../Elements/postData';

const Header = (props) => {

    const teamInfo = (team) => {
        return team ? (
            <TeamInfo team={team} />
        )
            :
            null
    };

    const postData = (date, author) => ( // 可以傳送一個 props 物件，包含所有要傳過去的東西
        <PostData data={{ date, author }} />
    )

    return (
        <div>
            {teamInfo(props.teamData)}
            {postData(props.date, props.author)}
        </div>
    )
};

export default Header;
