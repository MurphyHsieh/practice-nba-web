import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({
    user,
    component: Comp,
    ...rest // 其他來自route.js的 props 改成用 rest名字代替
}) => {
    return <Route {...rest} component={(props) => ( // 記得要寫成小括弧～
        rest.restricted ?
            ( // 有登入就不讓他進去 /sign-in 改去 /dashboard
                user ?
                    <Redirect to="/dashboard" />
                    :
                    <Comp {...props} user={user} />
            )
            :
            <Comp {...props} user={user} />
    )} />
}

export default PublicRoute;

