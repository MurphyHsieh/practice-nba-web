import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({
    user,
    component: Comp,
    ...rest // 其他來自route.js的 props 改成用 rest名字代替
}) => {
    return <Route {...rest} component={(props) => ( // 這裡的 props 是 <Route> 本身的 
        user ?
            <Comp {...props} user={user} />
            :
            <Redirect to="/sign-in" />
    )} />
}

export default PrivateRoute;

