import React from 'react';
import FontAwesome from 'react-fontawesome';
import styles from './cardinfo.css';
import moment from 'moment';

const CardInfo = (props) => {

    const teamName = (teams, team) => { // ES6 的新語法 .find 
        let data = teams.find((item) => { // item 會檢查每個 teams 的每一項，只會回傳相符的第一個
            return item.teamId === team
        });
        if (data) {
            return data.name
        }
    }

    const formatDate = (date) => {
        return moment(date).format(" MMM Do YY");

    }

    return (
        <div className={styles.cardInfo}>
            <span className={styles.teamName}>
                {teamName(props.teams, props.team)} {/* teamName() 可以宣告成 reusable component */}
            </span>
            <span className={styles.date}>
                <FontAwesome name="clock-o" />
                {formatDate(props.date)}
            </span>
        </div>
    )
};

export default CardInfo;
