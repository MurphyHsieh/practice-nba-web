import React, { Component } from 'react';
import { firebase } from '../../../firebase';
import FileUploader from 'react-firebase-file-uploader';

class Uploader extends Component {

    state = {
        name: '',
        isUploading: false,
        progress: 0,
        fileURL: ''
    }

    handleUploadStart = () => {
        this.setState({ isUploading: true, progress: 0 })
    }

    handleUploadError = (error) => {
        this.setState({ isUploading: false })
        console.log(error)
    }

    handleUploadSuccess = (filename) => { // 檔案成功上傳後，會得到檔案名字的回傳
        console.log(filename);
        this.setState({
            name: filename,
            progress: 100,
            isUploading: false
        })

        // 上傳成功後，還得將上傳的圖片顯示出來，
        // 所以必須向 firebase 索取網址
        firebase.storage().ref('images')  // 到 'images' (ref) 下試圖尋找(child) filename
            .child(filename).getDownloadURL()
            .then(url => {
                this.setState({ fileURL: url })
            })
        // 負責將 filenaem 回傳給 dashboard 透過 props
        this.props.filename(filename);
    }

    handleProgress = (progress) => {
        this.setState({ progress })
    }


    render() {
        return (
            <div>
                <FileUploader
                    accept="image/*" // * 代表任何形式的圖片
                    name="image"
                    randomizeFilename // 隨機產生圖片名字較安全！
                    storageRef={firebase.storage().ref('images')}
                    onUploadStart={this.handleUploadStart}
                    onUploadError={this.handleUploadError}
                    onUploadSuccess={this.handleUploadSuccess}
                    onProgress={this.handleProgress}
                />
                { /* 製作 progress bar 文件中有詳細的說明，也可以用其他方式製作(function) */
                    this.state.isUploading ?
                        <p>Progress: {this.state.progress}</p>
                        : null
                }
                { /* 成功上傳後，將上傳的圖片秀出來 */
                    this.state.fileURL ?
                        <img style={{
                            width: '300px'
                        }} src={this.state.fileURL} alt={this.state.fileURL} />
                        : null
                }
            </div>
        )
    }
};

export default Uploader;
