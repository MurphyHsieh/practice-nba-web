import React, { Component } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { Link } from 'react-router-dom';
import { firebaseTeams, firebaseLooper, firebaseArticles } from '../../../firebase';
import styles from './newsList.css';
import Button from '../Buttons/buttons';
import CardInfo from '../CardInfo/cardinfo';

class NewsList extends Component {

    state = {
        teams: [],
        items: [],
        start: this.props.start,
        end: this.props.start + this.props.amount,
        amount: this.props.amount
    }

    componentWillMount() {
        this.request(this.state.start, this.state.end)
    }

    request = (start, end) => {

        if (this.state.teams.length <= 0) {
            firebaseTeams.once('value')
                .then(snapshot => {
                    const teams = firebaseLooper(snapshot)
                    this.setState({
                        teams
                    })
                    // console.log(this.state.teams)
                })

            /* ========== without firebase ========== */

            // axios.get(`${URL}/teams`)
            //         .then(response => {
            //             this.setState({
            //                 teams: response.data
            //             })
            //         })    

        }

        firebaseArticles.orderByChild('id').startAt(start).endAt(end).once('value')
            .then(snapshot => {
                // console.log(snapshot.val())
                const articles = firebaseLooper(snapshot)
                this.setState({
                    items: [...this.state.items, ...articles],
                    start,
                    end
                })
            })
            .catch(e => {
                console.log(e)
            })



        // axios.get(`${URL}/articles?_start=${start}&_end=${end}`)
        //     .then(response => {
        //         this.setState({
        //             items: [...this.state.items, ...response.data], // 之後 loadmore 會需要用到
        //             start,
        //             end
        //         })
        //     })
    }

    loadMore = () => {
        let end = this.state.end + this.state.amount
        this.request(this.state.end + 1, end)
    }

    renderNews = (type) => {
        let templates = null
        switch (type) {            // switch 需要回傳東西
            case 'card':
                templates = this.state.items.map((item, i) => (
                    <CSSTransition
                        classNames={{
                            enter: styles.newsList_wrapper,
                            enterActive: styles.newsList_wrapper_enter
                        }}
                        timeout={500}
                        key={i}
                    >
                        <div>
                            <div className={styles.newslist_item}>
                                <Link to={`/articles/${item.id}`}>
                                    <CardInfo teams={this.state.teams} team={item.team} date={item.date} />
                                    <h2>{item.title}</h2>
                                </Link>
                            </div>
                        </div>
                    </CSSTransition>
                ))
                break;
            case 'cardMain':
                templates = this.state.items.map((item, i) => (
                    <CSSTransition
                        classNames={{
                            enter: styles.newsList_wrapper,
                            enterActive: styles.newsList_wrapper_enter
                        }}
                        timeout={500}
                        key={i}
                    >
                        <Link
                            to={`/articles/${item.id}`}
                        >
                            <div className={styles.flex_wrapper}>
                                <div className={styles.left}
                                    style={{
                                        background: `url('/images/articles/${item.image}')`
                                    }}
                                >
                                    <div></div> {/* 這個 div 控制背景圖片的大小！！ */}
                                </div>
                                <div className={styles.right}>
                                    <CardInfo teams={this.state.teams} team={item.team} date={item.date} />
                                    <h2>{item.title}</h2>
                                </div>
                            </div>
                        </Link>
                    </CSSTransition>
                ))
                break;
            default:
                templates = null
        }
        return templates;
    }

    render() {
        return (
            <div>
                <TransitionGroup
                    component="div"
                    className="list"
                >
                    {this.renderNews(this.props.type)}
                </TransitionGroup>

                <Button
                    type="loadmore"
                    loadMore={() => this.loadMore()} // loadMore 需要被呼叫 
                    cta="Load More News"
                />
            </div>
        )
    }
};

export default NewsList
