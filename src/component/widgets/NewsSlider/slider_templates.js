import React from 'react';
import Slick from 'react-slick';
import styles from './slider.css';
import { Link } from 'react-router-dom';

const SliderTemplates = (props) => {

    let templates = null;
    const setting = {
        dots: true,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        ...props.setting // 下面的展開後會覆寫上面的
    }

    switch (props.type) {
        case 'featured':
            templates = props.data.map((item, i) => { // 大括號記得要 return
                return ( // 每項都要有 key
                    <div key={i}>
                        <div className={styles.featured_item}>
                            <div className={styles.featured_image}
                                style={{ // 改成設定好的 image url
                                    background: `url('${item.image}')`
                                }}
                            ></div>
                            <Link to={`/articles/${item.id}`}>
                                <div className={styles.featured_caption}>
                                    {item.title}
                                </div>
                            </Link>

                        </div>
                    </div>
                )
            })
            break;
        default:
            templates = null;
    }

    return (
        <Slick {...setting}> {/* 透過 spread operator 把 setting 外包在引入！重要！ */}
            {templates}
        </Slick>
    )
};
export default SliderTemplates
