import React, { Component } from 'react';
import SliderTemplates from './slider_templates';
import { firebase, firebaseArticles, firebaseLooper } from '../../../firebase';

class NewsSlider extends Component {

    state = {
        news: [] // 宣告一個陣列 news 存放資料 atrticles
    }

    componentWillMount() {

        firebaseArticles.limitToFirst(3).once('value')
            .then((snapshot) => {

                const news = firebaseLooper(snapshot)

                // 第一種方式 替換 iameg url，缺點會setState三次，rerender 三次
                // news.forEach((item, i) => { // 得到 image 的網址
                //     firebase.storage().ref('images')
                //         .child(item.image).getDownloadURL()
                //         .then(url => {
                //             // 使用 map 替換東西的小技巧！！！！！
                //             news[i].image = url;
                //             this.setState({ news })
                //         })
                // })


                // 第二種方法，使用 promise 等待所有都完成替換 image url 後才去 setState！
                const asyncFunction = (item, i, cb) => {
                    firebase.storage().ref('images')
                        .child(item.image).getDownloadURL()
                        .then(url => {
                            // 使用 map 替換東西的小技巧！！！！！
                            news[i].image = url;
                            cb();
                        })
                }


                let requests = news.map((item, i) => {
                    return new Promise((resolve) => {
                        asyncFunction(item, i, resolve)
                    })
                })


                Promise.all(requests).then(() => {
                    this.setState({ news })
                })





            })
        /* ========== without firebase ========== */

        // axios.get(`${URL}/articles?_start=${this.props.start}&_end=${this.props.amount}`) // ?_start=0&_end=3 控制資料長度
        //     .then(response => {
        //         this.setState({
        //             news: response.data
        //         })
        //     })
    }

    render() {
        return (
            <div>
                <SliderTemplates data={this.state.news} type={this.props.type} setting={this.props.setting} />
            </div>
        )
    }
};

export default NewsSlider;
