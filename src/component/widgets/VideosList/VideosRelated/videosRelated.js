import React from 'react';
import styles from '../videosList.css';
import VideosListTemplate from '../videosListTemplate';

const VideoRelated = (props) => (
    <div className={styles.videoRelated_wrapper}>
        <VideosListTemplate
            data={props.data}
            teams={props.teams}
        />
    </div>
)

export default VideoRelated;
