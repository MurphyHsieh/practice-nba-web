import React, { Component } from 'react';

// customize component
import styles from './videosList.css';
import Button from '../Buttons/buttons';
import VideosTemplate from './videosListTemplate';
import { firebaseTeams, firebaseVideos, firebaseLooper } from '../../../firebase';

class videosList extends Component {

    state = {
        teams: [],
        videos: [],
        start: this.props.start,
        amount: this.props.amount,
        end: this.props.start + this.props.amount
    }

    renderTitle = () => { // 三元運算子
        return this.props.title ?
            <h3><strong>NBA</strong> Videos</h3>
            : null  // null 不想回傳東西！ 
    }

    componentWillMount() {
        this.request(this.state.start, this.state.end)
    }

    request(start, end) {

        if (this.state.teams.length < 1) {
            firebaseTeams.once('value')
                .then(snapshot => {
                    const teams = firebaseLooper(snapshot)
                    this.setState({
                        teams
                    })
                })

            /* ========== without firebase ========== */

            // fetch(`${URL}/teams`).then(response => response.json())
            //     .then(data => {
            //         console.log(data)
            //         this.setState({
            //             teams: data
            //         })
            //     })

            // axios.get(`${URL}/teams`)
            //     .then(response => {
            //         this.setState({
            //             teams: response.data
            //         })
            //     })

        }

        firebaseVideos.orderByChild('id').startAt(start).endAt(end).once('value')
            .then(snapshot => {
                // console.log(snapshot.val())
                const videos = firebaseLooper(snapshot)
                this.setState({
                    videos: [...this.state.videos, ...videos],
                    start,
                    end
                })
            })
            .catch(e => {
                console.log(e)
            })

        /* ========== without firebase ========== */

        // axios.get(`${URL}/videos?_start=${start}&_end=${end}`)
        //     .then(response => {
        //         this.setState({
        //             videos: [...this.state.videos, ...response.data],
        //             start,
        //             end
        //         })
        //     })
    }

    renderVideos = () => {
        let templates = null
        switch (this.props.type) {   // 示範 videos styles 模板外包的作法
            case 'card':
                templates = <VideosTemplate data={this.state.videos} teams={this.state.teams} />
                break;
            default:
                templates = null
        }
        return templates;
    }

    loadMore = () => {
        let end = this.state.end + this.state.amount;
        this.request(this.state.end + 1, end)
    }

    renderButton = () => {
        return this.props.loadmore ?
            <Button
                type="loadmore"
                loadMore={() => this.loadMore()} // loadMore 需要被呼叫
                cta="Load More Videos"
            />
            :
            <Button
                type="linkTo"
                cta="More videos"
                linkTo={"/videos"}
            />
    }


    render() {
        return (
            <div className={styles.videosList_wrapper}>
                {this.renderTitle()}
                {this.renderVideos()}
                {this.renderButton()}
            </div>
        )
    }
};

export default videosList;
