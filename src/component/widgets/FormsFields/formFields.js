import React from 'react';
import styles from './formFields.css';

const FormFields = ({ formdata, id, change }) => { // 只負責將畫面秀出來



    const showError = () => {
        let errorMessage = null;

        if (formdata.validation && !formdata.valid) {
            errorMessage = (
                <div className={styles.labelError}>
                    {formdata.validationMessage}
                </div>
            )
        }

        return errorMessage;
    }


    const renderTemplate = () => {
        let formTemplate = null;

        switch (formdata.element) {
            case ('input'):
                formTemplate = (
                    <div>
                        <input
                            {...formdata.config}
                            value={formdata.value}
                            onBlur={(event) => change({ event, id, blur: true })}

                            onChange={  // 如果一次要傳遞很多資料，接收方不想一次列舉太多參數空格：可以選擇傳遞一個物件即可
                                (event) => change({ event, id, blur: false })
                            }
                        />

                        {showError()}
                    </div>
                )
                break;
            case ('select'):
                formTemplate = (
                    <div>
                        <select
                            value={formdata.value}
                            name={formdata.config.name}
                            onBlur={(event) => change({ event, id, blur: true })}
                            onChange={(event) => change({ event, id, blur: false })}
                        >
                            {/* loop the teams pass from dashboard */}
                            {formdata.config.options.map((item, i) => (
                                <option
                                    key={i}
                                    value={item.id}
                                >
                                    {item.city}
                                </option>
                            ))}
                        </select>
                    </div>
                )
                break;
            default:
                formTemplate = null

        }
        return formTemplate;
    }

    return (
        <div>
            {renderTemplate()}
        </div>
    )
};

export default FormFields;