import React from 'react';
import { Link } from 'react-router-dom';
import styles from './buttons.css';
//import { URL } from '../../../config';

const Button = (props) => {

    let templates = null;
    switch (props.type) {
        case 'loadmore':
            templates = ( // 變數宣告後面加小括號，裡面可以直接寫 JSX 
                <div
                    className={styles.blue_btn}
                    onClick={props.loadMore}
                >
                    {props.cta}
                </div>
            )
            break;
        case "linkTo":
            templates = (
                <Link
                    to={props.linkTo}
                    className={styles.blue_btn}
                >
                    {props.cta}
                </Link>
            )
            break;
        default:
            templates = null;
    }

    return templates;
};

export default Button;
