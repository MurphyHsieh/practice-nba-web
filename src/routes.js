import React from 'react';

import { Switch } from 'react-router-dom';

import Home from './component/Home/home';
import Layout from './hoc/Layout/layout';

import NewsArticle from './component/Articles/News/Post/index';
import VideoArticle from './component/Articles/Videos/Video/index';
import NewsMain from './component/Articles/News/Main/index';
import VideoMain from './component/Articles/Videos/Main/index';
import SignIn from './component/SignIn/signin';
import Dashboard from './component/Dashboard/dashboard';

// 區分 private routes 與 public routes
import PrivateRoutes from './component/AuthRoutes/privateRoutes';
import PublicRoutes from './component/AuthRoutes/publicRoutes';

const Routes = (props) => {
    return (
        <Layout user={props.user}>
            <Switch>
                <PublicRoutes {...props} restricted={false} path="/" exact component={Home} />
                <PublicRoutes {...props} restricted={false} path="/news" exact component={NewsMain} />
                <PublicRoutes {...props} restricted={false} path="/videos" exact component={VideoMain} />
                <PublicRoutes {...props} restricted={false} path="/articles/:id" exact component={NewsArticle} />
                <PublicRoutes {...props} restricted={false} path="/videos/:id" exact component={VideoArticle} />
                <PublicRoutes {...props} restricted={true} path="/sign-in" exact component={SignIn} />
                <PrivateRoutes {...props} path="/dashboard" exact component={Dashboard} />
                {/* {...props} 包含 authentication user的部分，傳去給 <PrivateRoute> <PublicRoute> 判斷！ */}
            </Switch>
        </Layout>
    )

};

export default Routes;
